import scrapy
import datetime
from itertools import chain
from db_access import *


class SpiegelSpider(scrapy.Spider):

    name = 'spiegel'
    start_urls = ['https://www.spiegel.de/international/']

    def parse(self, response):
        db_access = DBAccess()
        db_access.connect()

        for article in chain(response.css('div.z-10'),
                             response.css('div.flex')):
            try:
                dt = datetime.datetime.now()
                scraped_info = {
                    'title':
                    article.css('a.text-black.block').attrib['title'],
                    'subtitle':
                    article.css(
                        'span.block.text-primary-base::text').get().strip(),
                    'abstract':
                    article.css('span.font-serifUI::text').get(),
                    'download_time':
                    dt.replace(tzinfo=None)
                }
                news_id = db_access.select_news(scraped_info['title'],
                                                scraped_info['subtitle'])
                if news_id:
                    db_access.update_news_time(dt.replace(tzinfo=None),
                                               news_id,
                                               scraped_info['abstract'])
                else:
                    db_access.insert_news(scraped_info['title'],
                                          scraped_info['subtitle'],
                                          scraped_info['abstract'],
                                          scraped_info['download_time'])
            except BaseException as e:
                print(e)
        # For crawling the next pages
        # next_page = response.css('span.outline-none').attrib['onclick']
        # if next_page is not None:
        # yield response.follow(next_page, callback=self.parse)
        db_access.disconnect()
