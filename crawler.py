from scrapy.crawler import CrawlerProcess
from postscrape.spiders.spiegel_spider import SpiegelSpider
import atexit
from apscheduler.schedulers.twisted import TwistedScheduler
from scrapy.utils.project import get_project_settings

atexit.register(lambda: sched.shutdown())

if __name__ == '__main__':
    process = CrawlerProcess(get_project_settings())
    sched = TwistedScheduler()
    sched.add_job(func=process.crawl,
                  trigger="interval",
                  args=[SpiegelSpider],
                  minutes=15)
    sched.start()
    process.crawl(SpiegelSpider)
    process.start(False)
