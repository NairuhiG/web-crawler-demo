# syntax=docker/dockerfile:1
FROM ubuntu:20.04

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN DEBIAN_FRONTEND="noninteractive" apt update && apt install -y python3-pip supervisor postgresql postgresql-contrib

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

# init db
RUN pg_ctlcluster 12 main start && su - postgres -c "psql -c \"alter user postgres with password '$(grep password database.ini | cut -d'=' -f 2)'\"" && su - postgres -c "psql -c \"create database $(grep database database.ini | cut -d'=' -f 2)\"" && su - postgres -c "psql -d $(grep database database.ini | cut -d= -f 2) -c \"create table tb_news (ID SERIAL PRIMARY KEY, title TEXT, subtitle TEXT, abstract TEXT, download_time timestamp, update_time timestamp)\"" && pg_ctlcluster 12 main stop


RUN mkdir -p /var/log/supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

CMD ["/usr/bin/supervisord"]
