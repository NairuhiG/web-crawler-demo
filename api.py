from flask import Flask, jsonify, render_template_string
from db_access import DBAccess
import json

app = Flask(__name__)


@app.route("/news/json")
def get_news_json():
    return get_news(html=False)


@app.route("/news")
def get_news(html=True):
    '''Test function to show all news'''
    db_access = DBAccess()
    db_access.connect()
    news_data = db_access.select_all_news()
    result = list()
    for row in news_data:
        news_dict = dict()
        news_dict['ID'] = row[0]
        news_dict['title'] = row[1]
        news_dict['subtitle'] = row[2]
        news_dict['abstract'] = row[3]
        news_dict['download_time'] = str(row[4])
        news_dict['update_time'] = str(row[5])
        result.append(news_dict)
    if html:
        return render_template_string('''


        <table>
                <tr>
                    <td> ID </td> 
                    <td> Title </td>
                    <td> Subtitle </td> 
                    <td> Abstract </td>
                    <td> Download Time </td> 
                    <td> Update Time </td>
                </tr>


        {% for news in result %}

                <tr>
                    <td>{{ news['ID'] }}</td> 
                    <td>{{ news['title'] }}</td>
                    <td>{{ news['subtitle'] }}</td>
                    <td>{{ news['abstract'] }}</td>
                    <td>{{ news['download_time'] }}</td>
                    <td>{{ news['update_time'] }}</td>
                </tr>

        {% endfor %}


        </table>
    ''',
                                      result=result)
    else:
        return jsonify(result)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
