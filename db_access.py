from re import sub
import psycopg2
from configparser import ConfigParser


class DBAccess():
    def __init__(self):
        self.conn = None

    def connect(self):
        """ Connect to the PostgreSQL database server """
        conn = None
        try:
            db_params = self.read_config()

            print('Connecting to the PostgreSQL database...')
            conn = psycopg2.connect(**db_params)
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        self.conn = conn

    def read_config(self, filename='database.ini', section='postgresql'):
        '''Reading configuration file'''

        parser = ConfigParser()
        parser.read(filename)

        db_config = {}
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                db_config[param[0]] = param[1]
        else:
            raise Exception('Section {0} not found in the {1} file'.format(
                section, filename))

        return db_config

    def disconnect(self):
        '''Closing db connection'''
        self.conn.close()

    def insert_news(self, title, subtitle, abstract, download_time):
        '''Inserting crawled news into db.'''
        # add try catch block for commit an finally conn close
        cur = self.conn.cursor()
        sql = """ INSERT INTO tb_news (title, subtitle, abstract, download_time) VALUES (%s, %s, %s, %s)"""
        t = (title, subtitle, abstract, download_time)
        cur.execute(sql, t)
        self.conn.commit()
        cur.close()

    def select_news(self, title, subtitle):
        '''Selecting existing news from db for further update or insertion.'''
        cur = self.conn.cursor()
        sql = """SELECT id FROM tb_news WHERE title = %s AND subtitle = %s"""
        t = (title, subtitle)
        cur.execute(sql, t)
        result = cur.fetchone()
        cur.close()
        return result

    def update_news_time(self, update_time, news_id, abstract):
        '''Updating the time for already existing news.'''
        cur = self.conn.cursor()
        sql = """UPDATE tb_news SET update_time = %s, abstract = %s  WHERE id = %s"""
        t = (update_time, abstract, news_id)
        cur.execute(sql, t)
        self.conn.commit()
        cur.close()

    def select_all_news(self):
        '''Selecting all news from the database'''
        cur = self.conn.cursor()
        sql = """SELECT * FROM tb_news ORDER BY id"""
        cur.execute(sql)
        result = cur.fetchall()
        cur.close()
        return result
