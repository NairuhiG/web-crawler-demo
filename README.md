# News Crawler

Crawling the Spiegel/international news and storing the results in the database with updating feature. 

<!--ts-->
1. [Requirements](#requirements)
2. [Key Assumptions](#key-assumptions)
3. [Technologies](#technologies)
4. [Description](#description)
5. [Run Docker Container](#run-docker-container)
<!--te-->

### **Requirements**

- Crawl URL: hhtps://www.spiegel.de/international
- Extract News-Entries from HTML
    - Title
    - Subtitle
    - Abstract
    - Download-time
- Store these entries in a suitable database
- The crawler should be triggered to run automatically every 15 min
- During re-runs existing entries should be detected and not stored as duplicates, but an additional timestamp should be stored: update_time

### **Key Assumptions**
- Created for demo purposes and focused on only on the requirements above.
- Limited only on crawling the first page of the news. Crawling over all other pages is also possible, nevertheless might be blocked for security reasons.
- Database credentials are not stored in the Python code but for demo purposes database.ini file can still be found in the project.
- The services(crawling, db, api) are separated, nevertheless for ease of testing they are built in a single container and managed by the supervisor daemon.
- Test are done manually.

### **Technologies**
- Python3
- Docker
- Scrapy
- PostgreSQL
- Flask



### **Description**

The code is written in Python3. It includes the following parts:
- crawler
- api
- spiegel_spider
- db_access


**crawler** - the code for managing web-crawler crawler.py.

**api** - developed for presenting data from database to user.

**spiegel_spider** - is developed for crawling the website and for writing crawled information to the database.

**db_access** - provides abstraction layer for database access and apply different queries(select, insert, update)

### **Run Docker Container:**

Running application in all-in-one Docker container(image hosted in dockerhub)
```
docker run --name demo -p 5000:5000 -d nairuhi/webcrawler
```

Error logs are redirected to stdout and can be monitored using 
```
docker logs demo
```

Output logs of each application are stored under /var/log/supervisor/ directories accordingly:
```
/var/log/supervisor/postgresql.log
/var/log/supervisor/crawler.log
/var/log/supervisor/api.log
```

Testing application results:
- View the full list in the browser
```
chrome http://<docker host ip>:5000/news
```
- Get news in json format using cli
```
curl http://<docker host ip>:5000/news/json
```

Building Docker container locally if need:
```
docker build --tag webcrawler .
```






